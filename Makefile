.PHONY: make-graph commit-forger

make-graph:
	make -C make-graph main

commit-forger:
	make -C commit-forger main

install:
	make make-graph
	mv make-graph/main ~/.local/bin/make-graph
	make commit-forger
	mv commit-forger/pcf ~/.local/bin/pcf
	cp commit-forger/commit-forger.sh ~/.local/bin/commit-forger
