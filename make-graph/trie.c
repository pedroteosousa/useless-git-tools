#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "commit.h"
#include "trie.h"

trie* create_trie() {
    trie* head = (trie *) malloc(sizeof (trie));
    head->info = NULL;
    for (int i = FIRST_CHAR; i <= LAST_CHAR; i++) {
        head->children[i - FIRST_CHAR] = NULL;
    }
}

void insert_commit(trie* head, commit* info) {
    trie* curr = head;
    char* name = info->name;
    while (*name) {
        if (*name < FIRST_CHAR || LAST_CHAR < *name) {
            fprintf(stderr, "Charater %c is invalid, "
                "should be in range [%c, %c]\n", *name, FIRST_CHAR, LAST_CHAR);
            return;
        }

        int curr_char = *name - FIRST_CHAR;
        if (curr->children[curr_char] == NULL) {
            curr->children[curr_char] = create_trie();
        }
        curr = curr->children[curr_char];
        name++;
    }
    curr->info = info;
}

commit* find_commit(trie* head, char *name) {
    trie* curr = head;
    while (*name) {
        curr = curr->children[*name - FIRST_CHAR];
        if (curr == NULL) {
            return NULL;
        }
        name++;
    }
    return curr->info;
}
