#ifndef LIST_H
#define LIST_H

typedef struct node {
    void* value;
    struct node* next;
} node;

node* create_node(void*);

typedef struct list {
    node* head;
    node* tail;
    int size;
} list;

list* create_list();

void append_value(list*, void*);

#endif // LIST_H
