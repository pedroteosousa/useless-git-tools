#include <stdio.h>
#include <stdlib.h>

#include "command.h"
#include "commit.h"
#include "list.h"

#define COMMAND_SIZE 100

command* create_command(commit* c, int type) {
    command* cmd = (command*) malloc(sizeof(command));
    cmd->c = c;
    cmd->type = type;
    return cmd;
}

void run_command(command* cmd) {
    const char* checkout_cmd = "git checkout %s %s";
    const char* commit_cmd = "touch .%s && git add . && git commit -m \"%s\"";
    const char* merge_cmd = "git merge %s --no-ff -m \"%s\"";

    int parents_str_size = cmd->c->parents->size * COMMIT_NAME_SIZE;
    char* parents_str = (char*) malloc(sizeof(char) * parents_str_size);
    int current_pos = 0;
    node* head = cmd->c->parents->head;
    while (head != NULL) {
        char* name = ((commit*)head->value)->name;
        while (*name) {
            parents_str[current_pos++] = *name;
            name++;
        }
        parents_str[current_pos++] = ' ';
        head = head->next;
    }
    parents_str[current_pos] = '\0';

    int cmd_str_size = COMMAND_SIZE + parents_str_size;
    char* cmd_str = (char*) malloc(sizeof(char) * cmd_str_size);
    
    if (cmd->type == CREATE_BRANCH) {
        sprintf(cmd_str, checkout_cmd, "-b", cmd->c->name);
    } else if (cmd->type == CHECKOUT) {
        sprintf(cmd_str, checkout_cmd, "", cmd->c->name);
    } else if (cmd->type == COMMIT) {
        sprintf(cmd_str, commit_cmd, cmd->c->name, cmd->c->name);
    } else if (cmd->type == MERGE) {
        sprintf(cmd_str, merge_cmd, parents_str, cmd->c->name);
    }
    
    system(cmd_str);
}
