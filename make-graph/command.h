#ifndef COMMAND_H
#define COMMAND_H

enum COMMAND_TYPE {
    CREATE_BRANCH, CHECKOUT, COMMIT, MERGE
};

typedef struct commit commit;

typedef struct command {
    commit* c;
    int type;
} command;

command* create_command(commit*, int);

void run_command(command*);

#endif // COMMAND_H
