#ifndef COMMIT_H
#define COMMIT_H

#define COMMIT_NAME_SIZE 51

enum QUEUE_STATE {
    NONE, WAITING, DONE
};

typedef struct list list;

typedef struct trie trie;

typedef struct commit {
    char *name;
    int state;
    list* parents;
    trie* parents_trie;
} commit;

commit* create_commit(char*);

#endif // COMMIT_H
