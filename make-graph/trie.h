#ifndef TRIE_H
#define TRIE_H

#define FIRST_CHAR '!'
#define LAST_CHAR '~'

typedef struct commit commit;

typedef struct trie {
    commit* info;
    struct trie* children[LAST_CHAR - FIRST_CHAR + 1];
} trie;

trie* create_trie();

void insert_commit(trie*, commit*);

commit* find_commit(trie*, char*);

#endif // TRIE_H
