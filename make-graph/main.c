#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "command.h"
#include "commit.h"
#include "list.h"
#include "trie.h"

bool search(commit* c, trie* targets, bool start, bool parent) {
    if (!start && !parent && find_commit(targets, c->name) != NULL) {
        return true;
    }

    bool found = false;
    node* curr = c->parents->head;
    while (curr != NULL && !found) {
        found |= search((commit*)curr->value, targets, false, start);
        curr = curr->next;
    }

    return found;
}

void generate_commands(commit* c, list* commands) {
    if (c->state == DONE) {
        return;
    }

    if (c->state == WAITING) {
        fprintf(stderr, "Found a cycle with commit '%s'\n", c->name);
        exit(1);
    }

    c->state = WAITING;

    node* curr = c->parents->head;
    while (curr != NULL) {
        generate_commands((commit*)curr->value, commands);
        curr = curr->next;
    }

    if (c->parents->size > 0) {
        append_value(commands, create_command((commit*)c->parents->head->value, CHECKOUT));
    }
    append_value(commands, create_command(c, CREATE_BRANCH));
    append_value(commands, create_command(c, c->parents->size > 1 ? MERGE : COMMIT));
    
    c->state = DONE;
}

int main() {
    trie* all_commits = create_trie();
    list* commits = create_list();

    printf("You are about to create a git repository in this folder.\n");
    printf("Are you sure you want to continue: [Y/N] ");
    char* input = (char*) malloc(sizeof(char) * 2);
    scanf("%s", input);
    if (input[0] != 'Y') {
        return 0;
    }

    // read graph
    commit* current_commit = NULL;
    char* name = (char*) malloc(sizeof(char) * COMMIT_NAME_SIZE);
    while (scanf("%s", name) != EOF) {
        bool is_child = name[strlen(name) - 1] == ':';
        if (is_child) {
            name[strlen(name) - 1] = '\0';
        }

        commit* c = find_commit(all_commits, name);
        if (c == NULL) {
            c = create_commit(name);
            append_value(commits, c);
            insert_commit(all_commits, c);
        }

        if (is_child) {
            current_commit = c;
        } else {
            append_value(current_commit->parents, c);
            if (current_commit->parents->size > 1) {
                insert_commit(current_commit->parents_trie, c);
            }
        }
    }

    // generate commands and validate graph
    int number_of_roots = 0;

    list* commands = create_list();

    node* curr = commits->head;
    while (curr != NULL) {
        commit* c = (commit*)curr->value;

        generate_commands(c, commands);

        // validate merges
        if (search(c, c->parents_trie, true, true)) {
            fprintf(stderr, "Invalid merge at commit \"%s\"\n", c->name);
            exit(1);
        }

        // validate roots
        if (c->parents->size == 0) {
            number_of_roots++;
        }

        curr = curr->next;
    }
    
    if (number_of_roots != 1) {
        fprintf(stderr, "Invalid number of roots. Found %d\n", number_of_roots);
        exit(1);
    }

    // run commands
    system("git init");
    curr = commands->head;
    while (curr != NULL) {
        command* cmd = (command*)curr->value;
        run_command(cmd);
        curr = curr->next;
    }
    return 0;
}
