#include <stdlib.h>
#include <string.h>

#include "list.h"

node* create_node(void* value) {
    node* n = (node*) malloc(sizeof(node));
    n->value = value;
    n->next = NULL;
    return n;
}

list* create_list() {
    list* l = (list*) malloc(sizeof(list));
    l->head = l->tail = NULL;
    l->size = 0;
    return l;
}

void append_value(list* l, void* value) {
    node* next_node = create_node(value);
    if (l->tail == NULL) {
        l->head = l->tail = next_node;
    } else {
        l->tail->next = next_node;
        l->tail = l->tail->next;
    }
    l->size++;
}
