#include <stdlib.h>
#include <string.h>

#include "commit.h"
#include "list.h"
#include "trie.h"

commit* create_commit(char *name) {
    commit* c = (commit*) malloc(sizeof(commit));
    
    c->name = (char*) malloc(sizeof(char) * COMMIT_NAME_SIZE);
    strcpy(c->name, name);
    
    c->state = NONE;

    c->parents = create_list();

    c->parents_trie = create_trie();

    return c;
}
