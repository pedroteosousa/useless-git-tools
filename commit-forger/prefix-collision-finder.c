#include <openssl/sha.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

int best = 0;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

const int MAX_THREADS = 8;

const int MAX_NUMBER_STRING_SIZE = 20;

struct brute_args {
	char* content_format;
	char* target_hash;
	int target_hash_size;
};

int get_match_size(char* altered_hash, char* target_hash) {
	int match_size = 0;
	while (altered_hash[match_size] == target_hash[match_size]) {
		match_size++;
	}
	return match_size;
}

char* hash_string(unsigned char* hash) {
	char* string = malloc(SHA_DIGEST_LENGTH * 2 + 1);
	for (int i = 0; i < SHA_DIGEST_LENGTH; i++) {
		sprintf(string + 2 * i, "%02x", hash[i]);
	}
	string[SHA_DIGEST_LENGTH * 2 + 1] = '\0';
	return string;
}

size_t format_commit(char** commit, char* content_format, int salt) {
	char* content = malloc((strlen(content_format) + MAX_NUMBER_STRING_SIZE) * sizeof(char));
	sprintf(content, content_format, salt);
	size_t content_size = strlen(content);

	const char* header_format = "commit %d";
	char* header = malloc(strlen(header_format) + MAX_NUMBER_STRING_SIZE);
	sprintf(header, header_format, content_size);
	size_t header_size = strlen(header);

	const char* commit_format = "%s%c%s";
	size_t commit_size = header_size + 1 + content_size;
	*commit = malloc(commit_size);
	sprintf(*commit, commit_format, header, '\0', content);

	free(content);
	free(header);

	return commit_size;
}

int get_salt_match_size(char* content_format, int salt, char* target_hash) {
	char* commit = NULL;
	size_t object_length = format_commit(&commit, content_format, salt);

	unsigned char hash[SHA_DIGEST_LENGTH];
	SHA1((unsigned char*)commit, object_length, hash);
	free(commit);

	char* altered_hash = hash_string(hash);
	int match_size = get_match_size(altered_hash, target_hash);
	free(altered_hash);

	return match_size;
}

void* brute_salt(void* args) {
	struct brute_args* arguments = (struct brute_args *)args;
	for (;;) {
		int salt = rand();
		int current = get_salt_match_size(arguments->content_format, salt, arguments->target_hash);

		pthread_mutex_lock(&mutex);
		if (best == arguments->target_hash_size) {
			pthread_mutex_unlock(&mutex);
			return NULL;
		}
		if (current > best) {
			best = current;
			printf("match of size %d with salt %d\n", current, salt);
		}
		pthread_mutex_unlock(&mutex);

	}
}

int main(int argc, char** argv) {
	srand(time(NULL));

	struct brute_args args;
	args.content_format = argv[1];
	args.target_hash = argv[2];
	args.target_hash_size = strlen(argv[2]);

	pthread_t *thread_group = malloc(sizeof(pthread_t) * MAX_THREADS);

	for (int i = 0; i < MAX_THREADS; i++) {
		pthread_create(&thread_group[i], NULL, &brute_salt, &args);
	}

	for (int i = 0; i < MAX_THREADS; i++) {
		pthread_join(thread_group[i], NULL);
	}

	return 0;
}
