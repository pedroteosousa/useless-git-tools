#!/bin/bash

function help() {
	echo "usage:"
	echo -e "\t$0 find <base commit> <target hash> [date]"
	echo -e "\t$0 create <base commit> <salt> [date]"
	exit 1
}

COMMAND=$1
case $COMMAND in
	"find")
		TARGET_HASH=$3
		;;
	"create")
		SALT=$3
		;;
	*)
		echo "error: no command named $1"
		help
		;;
esac

if [ $# -lt 3 ] || [ $# -gt 4 ]
then
	help
fi

COMMIT=$(git rev-parse "$2")

if [ ! -z "$4" ]
then
	DATE=$(date --date="$4" +"%s %z")
fi

COMMIT_CONTENT=$(git cat-file -p "$COMMIT")
if [ ! -z "$DATE" ]
then
	COMMIT_CONTENT=$(sed -E "s/(author .* <.*> ).*/\1$DATE/g" <<< "$COMMIT_CONTENT")
	COMMIT_CONTENT=$(sed -E "s/(committer .* <.*> ).*/\1$DATE/g" <<< "$COMMIT_CONTENT")
fi
case $COMMAND in
	"find")
		TARGET_HASH=$3
		COMMIT_CONTENT=$(perl -pe "s/(committer .*)\n/\1\nsalt \%d\n/g" <<< "$COMMIT_CONTENT")
		pcf "$COMMIT_CONTENT" "$TARGET_HASH"
		;;
	"create")
		SALT=$3
		COMMIT_CONTENT=$(perl -pe "s/(committer .*)\n/\1\nsalt "$SALT"\n/g" <<< "$COMMIT_CONTENT")
		printf "$COMMIT_CONTENT" | git hash-object --stdin -w -t commit
		;;
esac
